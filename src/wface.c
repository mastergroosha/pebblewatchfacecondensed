
#include <pebble.h>
static Window *s_main_window;
static TextLayer *s_time_layer;
static TextLayer *s_date_layer;
static GContext *ctx;

static GFont s_time_font;
static GFont s_date_font;

static char date_text[32];

void draw_text_sf(GContext* ctx,char* text, char* fnt, int x, int y,int w,int h,  GTextAlignment align){
	graphics_draw_text(ctx,text,fonts_get_system_font(fnt), GRect(x,y,w,h),GTextOverflowModeWordWrap,align,NULL);	
}

struct tm *get_time()
{
    time_t tt = time(0);
    return localtime(&tt);
}

typedef struct persist_datetime_lang { // 249 bytes
  char abbrDaysOfWeek[7][5];      //  21:  2 characters for each of  7 weekdays
  char monthsNames[12][20];       // 144: 11 characters for each of 12 months
  char DaysOfWeek[7][32];         //  84: 11 characters for each of  7 weekdays
//                                   249 bytes
} __attribute__((__packed__)) persist_datetime_lang;


static void update_time() {
  // Get a tm structure
  time_t temp = time(0); 
  struct tm *tick_time = get_time();
  
persist_datetime_lang lang_datetime = {
  .abbrDaysOfWeek = { "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" },
  .monthsNames = { "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря" },
  .DaysOfWeek = { "Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" },
};

  // Create a long-lived buffer
  static char buffer[] = "00:00";


    //snprintf(date_text, sizeof(date_text), "%s", lang_datetime.monthsNames[tick_time->tm_mon]);
  snprintf(date_text, sizeof(date_text), "%s, %i %s",lang_datetime.abbrDaysOfWeek[tick_time->tm_wday], tick_time->tm_mday, lang_datetime.monthsNames[tick_time->tm_mon]);
  //snprintf(date_text, sizeof(date_text), "%s", "bla-bla");
  
  
  // Write the current hours and minutes into the buffer
  if(clock_is_24h_style() == true) {
    // Use 24 hour format
    strftime(buffer, sizeof("00:00"), "%H:%M", tick_time);
  } else {
    // Use 12 hour format
    strftime(buffer, sizeof("00:00"), "%I:%M", tick_time);
  }

  // Display this time on the TextLayer
  //text_layer_set_text(s_date_layer, "Задай вопрос, на который можно ответить \"да\" или \"нет\" и нажми \n на кнопку -->"); 
  text_layer_set_text(s_date_layer, date_text);
  text_layer_set_text(s_time_layer, buffer);
}

  static void main_window_load(Window *window) {
  window_set_background_color(s_main_window, GColorBlack);
  s_time_layer = text_layer_create(GRect(5, 20, 139, 60));
  s_date_layer = text_layer_create(GRect(5, 82, 139, 60));
  
  //graphics_draw_text(ctx,date_text,fonts_get_system_font(FONT_KEY_GOTHIC_14), GRect(5,72,139,50),GTextOverflowModeWordWrap,GTextAlignmentCenter,NULL);
  //draw_text_sf(ctx, time_text,FONT_KEY_ROBOTO_BOLD_SUBSET_49, 0, -9,144,50, GTextAlignmentCenter);
 
    text_layer_set_background_color(s_time_layer, GColorBlack);
  text_layer_set_text_color(s_time_layer, GColorWhite);
  text_layer_set_background_color(s_date_layer, GColorBlack);
  text_layer_set_text_color(s_date_layer, GColorWhite);
  // Create GFont
  s_time_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_ROBOTO_48));
  s_date_font = fonts_get_system_font(FONT_KEY_GOTHIC_24);
  
  // Create time TextLayer
  text_layer_set_font(s_time_layer, s_time_font);
  text_layer_set_font(s_date_layer, s_date_font);
 // text_layer_set_text(s_time_layer, "00:00");
  
    
  // Improve the layout to be more like a watchface
 // text_layer_set_font(s_time_layer, fonts_load_custom_font(resource_get_handle(RESOURCE_ID_ROBOTO_48));
  text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);
  text_layer_set_text_alignment(s_date_layer, GTextAlignmentCenter);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_get_root_layer(window), text_layer_get_layer(s_time_layer));
  layer_add_child(window_get_root_layer(window), text_layer_get_layer(s_date_layer));
}

static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {

  update_time();
}


static void main_window_unload(Window *window) {
  // Unload GFont
    fonts_unload_custom_font(s_time_font);
    // Destroy TextLayer
    text_layer_destroy(s_time_layer);
    text_layer_destroy(s_date_layer);
}

static void init() {
  // Create main Window element and assign to pointer
  s_main_window = window_create();
  
    // Register with TickTimerService
  tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);

  // Set handlers to manage the elements inside the Window
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload
  });

  // Show the Window on the watch, with animated=true
  window_stack_push(s_main_window, true);
  
  
}

static void deinit() {
    // Destroy Window
    window_destroy(s_main_window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}